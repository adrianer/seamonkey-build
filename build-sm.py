#!/usr/bin/env python2.7
import argparse
import io
import logging
import os
import platform
import hashlib
import shutil
import subprocess
import threading
import time
import traceback
from ConfigParser import ConfigParser
from stat import ST_SIZE

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def is_linux():
    return "Windows" not in platform.system() and "Darwin" not in platform.system()
    

def is_mac():
    return "Darwin" in platform.system()


def is_windows():
    return "Windows" in platform.system()


def get_arch():
    if "64" in platform.architecture() or "64bit" in platform.architecture() or "64" in platform.processor():
        return 64
    elif is_mac():
        return 64
    else:
        return 32


def getFileHashAndSize(filepath):
    shaHash = 'UNKNOWN'
    size = 'UNKNOWN'
    try:
        # open in binary mode to make sure we get consistent results
        # across all platforms
        f = open(filepath, "rb")
        h = hashlib.sha512(f.read())
        shaHash = h.hexdigest()
        size = os.stat(filepath)[ST_SIZE]
    except IOError, (stderror):
       raise
    return (shaHash, size)


def exec_cmd(cmd):
    logger.info("Executing command: {0}".format(cmd))
    try:
        if is_windows():
            process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=None, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        else:
            process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=None)
        stdErrThread = threading.Thread(target=_stdErrReader, args=(process, ))
        stdErrThread.daemon = False
        stdErrThread.start()
        stdOutThread = threading.Thread(target=_stdOutReader, args=(process, ))
        stdOutThread.daemon = False
        stdOutThread.start()
        stdErrThread.join()
        stdOutThread.join()
    except Exception as e:
        logger.error("{0}".format(e))
        raise e


def _stdErrReader(process):
    stderrStream = io.open(process.stderr.fileno(), closefd=False, errors='ignore')
    for stderrData in stderrStream:
        logger.error(stderrData)
    stderrStream.close()


def _stdOutReader(process):
    stdoutStream = io.open(process.stdout.fileno(), closefd=False, errors='ignore')
    for stdoutData in stdoutStream:
        logger.info(stdoutData)
    stdoutStream.close()


def prepare_updates(locale, product, branch, repo_cat, objdir, force32bit):
    updates_dir = os.path.join(objdir, "dist", "update")
    files = os.listdir(updates_dir)
    en_us_update_filename = None
    en_us_update_pathname = None
    update_filename = None
    update_pathname = None
    for f in files:
        if "en-US" in f and f.endswith(".mar"):
            en_us_update_filename = f
            en_us_update_pathname = os.path.join(updates_dir, f)
            break
    for f in files:
        if ".{0}.".format(locale) in f and f.endswith(".mar"):
            update_filename = f
            update_pathname = os.path.join(updates_dir, f)
            break
    if en_us_update_filename and en_us_update_pathname and not (update_filename and update_pathname):
        update_filename = en_us_update_filename.replace("en-US", locale)
        update_pathname = en_us_update_pathname.replace("en-US", locale)
        # We have to rename the file, as it is named en-US no matter what the locale is
        if locale != "en-US":
            os.rename(en_us_update_pathname, update_pathname)
    if not (update_filename and update_pathname):
        raise ValueError("Nor MAR file found!")
    
    # We have to
    (hashSha, size) = getFileHashAndSize(update_pathname)
    c = ConfigParser()
    try:
        c.readfp(open(os.path.join(objdir, "dist", "bin", "application.ini")))
    except IOError, (stderror):
       raise
    build_id = c.get("App", "BuildID")
    app_version = c.get("App", "Version")
    update_xml = """
<updates>
<update type="minor" displayVersion="{0}" appVersion="{0}" platformVersion="{0}" version="{0}" extensionVersion="{0}" buildID="{1}" detailsURL="https://l10n.mozilla-community.org/~akalla/unofficial/{6}/nightly/latest-{9}-{5}-{7}{8}/" showPrompt="true">
<patch type="complete" URL="https://l10n.mozilla-community.org/~akalla/unofficial/{6}/nightly/latest-{9}-{5}-{7}{8}/{2}" hashFunction="SHA512" hashValue="{3}" size="{4}"/>
</update>
</updates>
"""
    bit = 64 if get_arch() is 64 and not force32bit else 32
    operating_system = platform.system().lower() if not is_mac() else "mac"
    update_xml = update_xml.format(app_version, build_id, update_filename, hashSha, size, branch, product.lower(), operating_system, bit, repo_cat)
    upate_xml_dirpath = os.path.join(updates_dir, locale)
    if not os.path.exists(upate_xml_dirpath):
        os.makedirs(upate_xml_dirpath)
    with open(os.path.join(upate_xml_dirpath, "update.xml"), "w") as xml_fh:
        xml_fh.write(update_xml)


def prepare_mozconfig(target, objdir, force32bit, calendar, debug, channel, branch):
    with open(".mozconfig", "w") as fh:
        if is_windows() and get_arch() is 64 and not force32bit:
            fh.write("ac_add_options --target=x86_64-pc-mingw32\n")
            fh.write("ac_add_options --host=x86_64-pc-mingw32\n")
        if is_linux():
            # Self-compiled GCC 4.9.4:
            # wget ftp://ftp.fu-berlin.de/unix/languages/gcc/releases/gcc-4.9.4/gcc-4.9.4.tar.bz2 && tar xfvj gcc-4.9.4.tar.bz2
            # cd gcc-4.9.4/ && ./configure --prefix=/opt/gcc-4.9.4 --with-multilib-list=m32,m64 && make -j 5 && sudo make install && cd ..
            fh.write("CC=/opt/gcc-4.9.4/bin/gcc\n")
            fh.write("CXX=/opt/gcc-4.9.4/bin/g++\n")
        if is_linux() and get_arch() is 64 and force32bit:
            fh.write("CC=\"$CC -m32 -march=pentiumpro\"\n")
            fh.write("CXX=\"$CXX -m32 -march=pentiumpro\"\n")
            fh.write("ac_add_options --target=i686-pc-linux\n")
            fh.write("ac_add_options --host=i686-pc-linux\n")
            fh.write("ac_add_options --x-libraries=/usr/lib\n")
            # CROSS_COMPILE=1 breaks everything...
            # fh.write("export CROSS_COMPILE=1\n")
            fh.write("export PKG_CONFIG_PATH=/usr/lib/pkgconfig")
        if not is_windows():
            fh.write('mk_add_options MOZ_MAKE_FLAGS="-s -j4"\n')
        else:
            fh.write('mk_add_options MOZ_MAKE_FLAGS="-j4"\n')
        if is_mac():
            fh.write('ac_add_options --with-macos-sdk=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.7.sdk\n')
            fh.write('mk_add_options AUTOCONF=/usr/local/Cellar/autoconf213/2.13/bin/autoconf213\n')
        fh.write("ac_add_options --enable-application={0}\n".format(target))
        if calendar:
            fh.write("ac_add_options --enable-calendar\n")
        if is_windows():
            fh.write('mk_export_correct_style() {\n  mk_add_options "export $1=$(cmd.exe //c echo %$1%)"\n}\n')  # Pymake needs Windows-style paths. Use cmd.exe to hack around this.
            fh.write('VSPATH="/c/Program Files (x86)/Microsoft Visual Studio 14.0"\n')
            fh.write('WINDOWS_SDK_DIR="/c/Program Files (x86)/Windows Kits/10/"\n')
            fh.write('# export WINDOWSSDKDIR="${WINDOWS_SDK_DIR}"\n')
            fh.write('\n')
            fh.write('\n')
            fh.write('\n')
            if get_arch() is 64 and not force32bit:
                fh.write('export WIN32_REDIST_DIR="${VSPATH}/VC/redist/x64/Microsoft.VC140.CRT"\n')
                fh.write('export WIN_UCRT_REDIST_DIR="${WINDOWS_SDK_DIR}/Redist/ucrt/DLLs/x64"\n')
                fh.write('export PATH="${VSPATH}/VC/bin/amd64:${VSPATH}/VC/bin:${WINDOWS_SDK_DIR}/bin/x64:${VSPATH}/VC/redist/x64/Microsoft.VC140.CRT:${WINDOWS_SDK_DIR}/Redist/ucrt/DLLs/x64:${VSPATH}/DIASDK/bin/amd64:${PATH}"\n')
                fh.write('export INCLUDE="${VSPATH}/VC/include:${VSPATH}/VC/atlmfc/include:${WINDOWS_SDK_DIR}Include/10.0.10586.0/ucrt:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/shared:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/um:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/winrt:${VSPATH}/DIASDK/include"\n')
                fh.write('export LIB="${VSPATH}/VC/lib/amd64:${VSPATH}/VC/atlmfc/lib/amd64:${WINDOWS_SDK_DIR}/Lib/10.0.10586.0/ucrt/x64:${WINDOWS_SDK_DIR}/Lib/10.0.10586.0/um/x64:${VSPATH}/DIASDK/lib/amd64"\n')
            else:
                fh.write('export WIN32_REDIST_DIR="${VSPATH}/VC/redist/x86/Microsoft.VC140.CRT"\n')
                fh.write('export WIN_UCRT_REDIST_DIR="${WINDOWS_SDK_DIR}/Redist/ucrt/DLLs/x86"\n')
                fh.write('export PATH="${VSPATH}/VC/bin/amd64_x86:${VSPATH}/VC/bin:${WINDOWS_SDK_DIR}/bin/x86:${VSPATH}/VC/redist/x86/Microsoft.VC140.CRT:${WINDOWS_SDK_DIR}/Redist/ucrt/DLLs/x86:${VSPATH}/DIASDK/bin:${PATH}"\n')
                fh.write('export INCLUDE="${VSPATH}/VC/include:${VSPATH}/VC/atlmfc/include:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/ucrt:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/shared:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/um:${WINDOWS_SDK_DIR}/Include/10.0.10586.0/winrt:${VSPATH}/DIASDK/include"\n')
                fh.write('export LIB="${VSPATH}/VC/lib:${VSPATH}/VC/atlmfc/lib:${WINDOWS_SDK_DIR}/Lib/10.0.10586.0/ucrt/x86:${WINDOWS_SDK_DIR}/Lib/10.0.10586.0/um/x86:${VSPATH}/DIASDK/lib"\n')
            fh.write('mk_export_correct_style INCLUDE\n')
            fh.write('mk_export_correct_style LIB\n')
            fh.write('mk_export_correct_style PATH\n')
            fh.write('mk_export_correct_style WIN32_REDIST_DIR\n')
            fh.write('mk_export_correct_style WIN_UCRT_REDIST_DIR\n')
        fh.write("mk_add_options MOZ_OBJDIR={0}\n".format(objdir))
        fh.write("# ac_add_options --enable-official-branding\n")
        if debug:
            fh.write("ac_add_options --enable-debug\n")
        else:
            if not is_windows():
                fh.write("ac_add_options --enable-optimize\n")
            else:
                fh.write("ac_add_options --enable-optimize=-O2\n")
        fh.write("ac_add_options --disable-tests\n")
        # RUST installed by 'rustup': https://www.rustup.rs
        # curl https://sh.rustup.rs -sSf | sh
        if is_mac():
            fh.write('RUSTC="/Users/mozilla/.cargo/bin/rustc"\n')
            fh.write('CARGO="/Users/mozilla/.cargo/bin/cargo"\n')
        if is_linux():
            if get_arch() is 32 or force32bit:
                fh.write('RUSTC="/home/mozilla/.rustup/toolchains/stable-i686-unknown-linux-gnu/bin/rustc"\n')
                fh.write('CARGO="/home/mozilla/.rustup/toolchains/stable-i686-unknown-linux-gnu/bin/cargo"\n')
            else:
                fh.write('RUSTC="/home/mozilla/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin/rustc"\n')
                fh.write('CARGO="/home/mozilla/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin/cargo"\n')
            fh.write("ac_add_options --enable-stdcxx-compat\n")
            fh.write("ac_add_options --with-ccache=/usr/bin/ccache\n")
        if is_windows():
            if get_arch() is 32 or force32bit:
                fh.write('RUSTC="c:/Users/Administrator/.rustup/toolchains/stable-i686-pc-windows-msvc/bin/rustc.exe"\n')
                fh.write('CARGO="c:/Users/Administrator/.rustup/toolchains/stable-i686-pc-windows-msvc/bin/cargo.exe"\n')
            else:
                fh.write('RUSTC="c:/Users/Administrator/.rustup/toolchains/stable-x86_64-pc-windows-msvc/bin/rustc.exe"\n')
                fh.write('CARGO="c:/Users/Administrator/.rustup/toolchains/stable-x86_64-pc-windows-msvc/bin/cargo.exe"\n')
            fh.write("ac_add_options --enable-jemalloc\n")
            fh.write("ac_add_options --disable-signmar\n")
            if branch == "esr":
                if get_arch() is 32 or force32bit:
                    fh.write("ac_add_options --enable-require-all-d3dc-versions\n")
            fh.write("ac_add_options --enable-profiling\n")
            fh.write("export INCLUDE=$INCLUDE:/c/Office\ 2010\ Developer\ Resources/Outlook\ 2010\ MAPI\ Headers\n")
            fh.write("mk_export_correct_style INCLUDE\n")
        fh.write("ac_add_options --enable-update-channel={0}\n".format(channel))
        fh.write("mk_add_options AUTOCLOBBER=1\n")
        fh.write("ac_add_options --enable-crashreporter\n")
        fh.write("ac_add_options --enable-release\n")
        fh.write("ac_add_options --enable-js-shell\n")
        fh.write("MOZ_ADDON_SIGNING=0\n")  # Disable checking that add-ons are signed by the trusted root
        fh.write("MOZ_REQUIRE_SIGNING=0\n")  # Disable enforcing that add-ons are signed by the trusted root
        fh.write("unset SOCORRO_SYMBOL_UPLOAD_TOKEN_FILE\n")
        fh.write("export MOZ_DEBUG_SYMBOLS=1\n")
        fh.write("export MOZILLA_OFFICIAL=1\n")  # Needed to enable breakpad in application.ini
        fh.write("export MOZ_TELEMETRY_REPORTING=1\n")  # For NSS symbols


def override_update_server(pref_js_path):
    old_update_pref = r'pref("app.update.url",'
    new_update_pref = 'pref("app.update.url", "{0}");'
    new_update_url = r"https://l10n.mozilla-community.org/~akalla/unofficial/update/3/%PRODUCT%/%CHANNEL%/%BUILD_TARGET%/%LOCALE%/update.xml"

    # List of possible official update servers
    update_servers_official = ("aus2-community.mozilla.org", "aus3.mozilla.org", "aus4.mozilla.org")

    browser_prefs_js_lines = []
    with open(pref_js_path, "r") as fh:
        for line in fh:
            browser_prefs_js_lines.append(line)

    # Patch the certificates to the ones used by our own update server 
    with open(pref_js_path, "w") as fh:
        for line in browser_prefs_js_lines:
            if line.startswith(old_update_pref):
                fh.write(new_update_pref.format(new_update_url))
            elif 'commonName",' in line:
                for official_update_server in update_servers_official:
                    if official_update_server in line:
                        fh.write(line.replace(official_update_server, "l10n.mozilla-community.org"))
                        break
                else:
                    fh.write(line)
            elif "OU=Equifax Secure Certificate Authority,O=Equifax,C=US" in line:
                fh.write(line.replace("OU=Equifax Secure Certificate Authority,O=Equifax,C=US", "CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US"))
            elif r'CN=GeoTrust SSL CA,O=\"GeoTrust, Inc.\",C=US' in line:
                fh.write(line.replace(r'CN=GeoTrust SSL CA,O=\"GeoTrust, Inc.\",C=US', "CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US"))
            elif r'CN=Thawte SSL CA,O=\"Thawte, Inc.\",C=US' in line:
                fh.write(line.replace(r'CN=Thawte SSL CA,O=\"Thawte, Inc.\",C=US', "CN=l10n.mozilla-community.org,O=Mozilla Corporation,C=US"))
            else:
                fh.write(line)


def main(target='suite', branch='central', release_tag=None, debug=False, calendar=False, inspector_tag=None, chatzilla_tag=None, force32bit=False, pull=True, build=True, l10n_repack=True, clobber=True, applypatches=True):
    # jenkins_build_url = os.environ.get("BUILD_URL")

    #####################################################################################################
    # Prepare variables for later use
    #
    bit = 64 if get_arch() is 64 and not force32bit else 32

    # Set correct channels and urls
    channel = branch
    if branch == 'central':
        channel = "nightly"
        if target == 'browser':
            repo_url = 'https://hg.mozilla.org/mozilla-central'
        else:
            repo_url = 'https://hg.mozilla.org/comm-central'
        l10n_url = 'https://hg.mozilla.org/l10n-central'
    elif branch == 'beta':
        if target == 'browser':
            repo_url = 'https://hg.mozilla.org/releases/mozilla-beta'
        else:
            repo_url = 'https://hg.mozilla.org/releases/comm-beta'
        l10n_url = 'https://hg.mozilla.org/releases/l10n/mozilla-beta'
    elif branch == 'release':
        if target == 'browser':
            repo_url = 'https://hg.mozilla.org/releases/mozilla-release'
        else:
            repo_url = 'https://hg.mozilla.org/releases/comm-release'
        l10n_url = 'https://hg.mozilla.org/releases/l10n/mozilla-release'
    elif branch == 'esr':
        if target == 'browser':
            repo_url = 'https://hg.mozilla.org/releases/mozilla-esr52'
        else:
            repo_url = 'https://hg.mozilla.org/releases/comm-esr52'
        l10n_url = 'https://hg.mozilla.org/releases/l10n/mozilla-release'
    else:
        raise Exception('Unknown branch {0}!'.format(branch))

    # is comm or mozilla our main repo?
    if target == 'browser':
        repo_cat = "mozilla"
    else:
        repo_cat = "comm"

    # Product names and pref.js paths
    if target == "suite":
        product = "SeaMonkey"
        pref_js_path = os.path.join("suite", "browser", "browser-prefs.js")
    elif target == "mail":
        product = "Thunderbird"
        pref_js_path = os.path.join("mail", "app", "profile", "all-thunderbird.js")
    elif target == "browser":
        product = "Firefox"
        pref_js_path = os.path.join("browser", "app", "profile", "firefox.js")
    else:
        raise Exception("Unknown target {0}!".format(target))

    # make command to use
    if is_windows():
        make = "mozmake"
    else:
        make = "make"

    # mach command to use
    if is_windows():
        if repo_cat == "comm":
            mach = "sh mozilla/mach"
        else:
            mach = "sh mach"
    else:
        if repo_cat == "comm":
            mach = "./mozilla/mach"
        else:
            mach = "./mach"

    # set our buid, merge and obj-dirs
    build_dir = os.path.dirname(os.path.abspath(__file__))
    if is_windows():
        # :TODO: wget the patched mozilla-build batch files
        build_dir = build_dir.lower()
        merge_dir = "C:\\\\locales-merge"
        if bit is 64:
            #objdir = os.path.join(build_dir, "objdir").replace("\\", "\\\\\\\\")
            objdir = os.path.join(build_dir, "obj-x86_64-pc-mingw32").replace("\\", "\\\\")
        else:
            #objdir = os.path.join(build_dir, "objdir")
            objdir = os.path.join(build_dir, "obj-i686-pc-mingw32").replace("\\", "\\\\")
    else:
        merge_dir = "/tmp/locales-merge"
        objdir = os.path.join(build_dir, "objdir")

    if is_mac():
        wget_bin = "/usr/local/bin/wget"
    else:
        wget_bin = "wget"

    hg_bin = "hg --config hostfingerprints.hg.mozilla.org=73:7f:ef:ab:68:0f:49:3f:88:91:f0:b7:06:69:fd:8f:f2:55:c9:56"
    if is_mac():
        hg_bin = "/usr/local/bin/{0}".format(hg_bin)
    #####################################################################################################

    if pull:
        if os.path.exists(".hg"):
            exec_cmd("{0} pull".format(hg_bin))
            if release_tag is None:
                exec_cmd("{0} update -C".format(hg_bin))
            else:
                exec_cmd("{0} update -C {1}".format(hg_bin, release_tag))
        else:
            # :FIXME: Cloning in this dir will not work, as it is not empty,
            # as our script is already here...
            # For now: Jenkins should clone the repo before using this script!!!
            exec_cmd("{0}  clone {1} .".format(hg_bin, repo_url))
            if release_tag is not None:
                exec_cmd("{0} update -C {1}".format(hg_bin, release_tag))

        ###################################################################################################################################################
        # Patches for bugs. Here only patches that need to be applied *before* invoking client.py

        #if applypatches:
        #    # Bug 1167346: pull the right Chatzilla and DOMi versions instead of seriously old ones
        #    if target == "suite" and branch != "central":
        #        exec_cmd("{0} --no-check-certificate -O bug1167346.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/bug1167346.patch".format(wget_bin))
        #        exec_cmd("patch -F 9 -p 1 -i bug1167346.patch")

        ###################################################################################################################################################

        # Execute client.py
        if is_windows():
            client_py_cmd = "python2.7 client.py"
        if is_linux():
            client_py_cmd = "/usr/bin/env python2.7 client.py"
        if is_mac():
            client_py_cmd = "/usr/bin/env python2.7 client.py --hg=/usr/local/bin/hg"
        if release_tag:
            client_py_cmd += " -r {0} --ldap-rev=default".format(release_tag)
        if inspector_tag:
            client_py_cmd += " --inspector-rev {0}".format(inspector_tag)
        if chatzilla_tag:
            client_py_cmd += " --chatzilla-rev {0}".format(chatzilla_tag)
        client_py_cmd = "{0} checkout".format(client_py_cmd)
        exec_cmd(client_py_cmd)

        ###################################################################################################################################################
        # Patches for bugs

        if applypatches:
            # Bug 1266123 workaround + patch for Bug 1279027. Otherwise building on Windows fails...
            exec_cmd("{0} --no-check-certificate -O bug1266123_workaround_bug1279027.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/bug1266123_workaround_bug1279027.patch".format(wget_bin))
            if repo_cat == "comm":
                exec_cmd("patch -d {0} -F 5 -p 1 -i {1}".format(os.path.join(build_dir, "mozilla"), os.path.join(build_dir, "bug1266123_workaround_bug1279027.patch")))
            else:
                exec_cmd("patch -F 5 -p 1 -i bug1266123_workaround_bug1279027.patch")

            if is_linux():
                # Bug 1179805: this bug needs to be workarounded on every Linux platform other than CentOS6...
                exec_cmd("{0} --no-check-certificate -O bug1179805_workaround.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/bug1179805_workaround.patch".format(wget_bin))
                if repo_cat == "comm":
                    exec_cmd("patch -d {0} -p 1 -i {1}".format(os.path.join(build_dir, "mozilla"), os.path.join(build_dir, "bug1179805_workaround.patch")))
                else:
                    exec_cmd("patch -p 1 -i bug1179805_workaround.patch")

                # undefined reference to atk_bridge_adaptor_init patch
                # exec_cmd("{0} --no-check-certificate -O undefined_reference_to_atk_bridge_adaptor_init.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/undefined_reference_to_atk_bridge_adaptor_init.patch".format(wget_bin))
                # if repo_cat == "comm":
                #     exec_cmd("patch -d {0} -p 1 -i {1}".format(os.path.join(build_dir, "mozilla"), os.path.join(build_dir, "undefined_reference_to_atk_bridge_adaptor_init.patch")))
                # else:
                #     exec_cmd("patch -p 1 -i undefined_reference_to_atk_bridge_adaptor_init.patch")

            if target == "suite":
                # Bug 1231349: Workaround for L10n repacks broken on with SM 2.42 and newer
                exec_cmd("{0} --no-check-certificate -O bug1231349_workaround.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/bug1231349_workaround.patch".format(wget_bin))
                exec_cmd("patch -d {0} -F 5 -p 1 -i {1}".format(os.path.join(build_dir, "mozilla"), os.path.join(build_dir, "bug1231349_workaround.patch")))

                # Bug 485871: ChatZilla should use the python version of compare-locales
                exec_cmd("{0} --no-check-certificate -O chatzilla_compare_part1.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/chatzilla_compare_part1.patch".format(wget_bin))
                exec_cmd("patch -d {0} -F 5 -p 1 -i {1}".format(os.path.join(build_dir, "mozilla", "extensions", "irc"), os.path.join(build_dir, "chatzilla_compare_part1.patch")))
                exec_cmd("{0} --no-check-certificate -O chatzilla_compare_part2.patch https://bitbucket.org/adrianer/seamonkey-build/raw/default/chatzilla_compare_part2.patch".format(wget_bin))
                exec_cmd("patch -F 5 -p 1 -i chatzilla_compare_part2.patch")

        ###################################################################################################################################################

        # ESR L10n changesets
        l10n_changesets = {
            "cs": "392a1ff68cfd",
            "de": "fab411f83a79",
            "en-GB": "3b1783bdb6fd",
            "es-AR": "313669f76edb",
            "es-ES": "3debdad3d876",
            "fr": "7e9efb5f39b1",
            "hu": "7ec46b30e96d",
            "it": "364ba1b94d4d",
            "ja": "289cc8d9b658",
            "ja-jp-mac": "1dc42d8f8c6b",
            "lt": "1c563aa726f8",
            "nl": "b3c2ab953068",
            "pl": "e60eff6e41c3",
            "pt-PT": "5deb6216933d",
            "ru": "3d319dcf82c7",
            "sk": "968ee9fbd8ba",
            "sv-SE": "2d93a3f0c284",
            "zh-CN": "d3ccc79b70b2",
            "zh-TW": "4b95fb19a582"
        }

        # clone/pull all locales
        with open("{0}/locales/all-locales".format(target), "r") as fh:
            locale = fh.readline().strip()
            while locale:
                if os.path.exists("locales/{0}".format(locale)):
                    exec_cmd("{0} pull -R locales/{1}".format(hg_bin, locale))
                    exec_cmd("{0} update -C -R locales/{1}".format(hg_bin, locale))
                else:
                    if not os.path.exists("locales"):
                        os.makedirs("locales")
                    exec_cmd("{0} clone {2}/{1} locales/{1}".format(hg_bin, locale, l10n_url))

                if branch == 'esr' and locale in l10n_changesets:
                    try:
                        exec_cmd("{0} update -r {2} -R locales/{1}".format(hg_bin, locale, l10n_changesets[locale]))
                    except Exception as e:
                        # Release tag changeset does not exist for this locale
                        logger.warn("Changeset '{0}' was not found for lcoale '{1}'. Falling back to default.".format(release_tag, locale))

                if release_tag is not None:
                    try:
                        exec_cmd("{0} update -C -R locales/{1} {2}".format(hg_bin, locale, release_tag))
                    except Exception as e:
                        # Release tag probably does not exist for this locale
                        logger.warn("Release tag '{0}' was not found for lcoale '{1}'. Falling back to default.".format(release_tag, locale))
                        exec_cmd("{0} update -C -R locales/{1}".format(hg_bin, locale))
                locale = fh.readline().strip()

    if build:
        if clobber:
            # :FIXME: Do it properly using the clobber-commands
            exec_cmd("{0} clobber".format(mach))
            if os.path.exists("objdir"):
                shutil.rmtree("objdir", ignore_errors=True)        
            if os.path.exists(objdir):
                shutil.rmtree(objdir, ignore_errors=True)
            
            if os.path.exists(os.path.join("mozilla", objdir)):
                shutil.rmtree(os.path.join("mozilla", objdir), ignore_errors=True)
            if not is_windows() and os.path.exists(os.path.join("mozilla", objdir)):
                # ok, this is probably a symlink
                os.remove(os.path.join("mozilla", objdir))

        # prepare our .mozconfig
        prepare_mozconfig(target, objdir, force32bit, calendar, debug, channel, branch)

        # override the update server
        override_update_server(pref_js_path)
    
        # LOCALE_MERGEDIR should not be set for the en-US build
        os.unsetenv("LOCALE_MERGEDIR")
    
        # workarounds for build bugs:
        if not os.path.exists(objdir):
            os.makedirs(objdir)
        if not os.path.exists("mozilla"):
            os.makedirs("mozilla")
        if not is_windows() and os.path.exists(objdir) and not os.path.exists(os.path.join("mozilla", objdir)):
            os.symlink(os.path.join("..", objdir), os.path.join("mozilla", objdir))

        # Build and create en-US packages
        exec_cmd("{0} configure".format(mach))
        exec_cmd("{0} build".format(mach))
        exec_cmd("{0} buildsymbols".format(mach))
        # exec_cmd("{0} -C {1} uploadsymbols".format(make, objdir))
        exec_cmd("{0} package".format(mach))
        # :FIXME: Is the below line still needed, of does mach package also create e.g. the Windows installer?
        exec_cmd("{0} -C {1} installer".format(make, objdir))

    if l10n_repack:
        # Update our mozconfig file in preparation of the L10n-process
        with open(".mozconfig", "a") as fh:
            fh.write("ac_add_options --with-l10n-base=../locales/\n")
            fh.write("export LOCALE_MERGEDIR={0}\n".format(merge_dir))
        
        exec_cmd("{0} configure".format(mach))

        # read the RemotingName and Version from application.ini
        # We need this to create the update snippets later
        c = ConfigParser()
        try:
            c.readfp(open(os.path.join(objdir, "dist", product.lower(), "application.ini")))
            name_id = c.get("App", "RemotingName")
            app_version = c.get("App", "Version")
        except IOError, (stderror):
            name_id = None
            app_version = None

        # Locales for which we do full repacks. All other locales will only get a langpack
        all_full_locales = ("de", "es-ES", "fr", "it", "pl", "pt-BR")

        # Do the L10n repacks
        with open("{0}/locales/all-locales".format(target), "r") as fh:
            locale = fh.readline().strip()
            while locale:
                if os.path.exists(merge_dir):
                    shutil.rmtree(merge_dir)
                try:
                    # clobber first (needed if a previous clobber did not succeed)
                    exec_cmd("{2} -C {3}/{1}/locales clobber-zip AB_CD={0}".format(locale, target, make, objdir))
                    # L10n-merge (mandatory)
                    exec_cmd("{2} -C {3}/{1}/locales merge-{0} LOCALE_MERGEDIR={4}".format(locale, target, make, objdir, merge_dir))
                    exec_cmd("{2} -C {3}/{1}/locales langpack-{0} LOCALE_MERGEDIR={4}".format(locale, target, make, objdir, merge_dir))
                    if locale in all_full_locales:
                        exec_cmd("{2} -C {3}/{1}/locales installers-{0} LOCALE_MERGEDIR={4}".format(locale, target, make, objdir, merge_dir))
                        # Create the mar-file for this locale
                        if is_windows():
                            exec_cmd("{0} -C {1}/tools/update-packaging complete-patch PACKAGE_BASE_DIR={1}/dist/l10n-stage AB_CD={2} UNPACKAGE={3}".format(make, objdir, locale, "{0}/dist/install/sea/{1}-{2}.{3}.win{4}.installer.exe".format(objdir, name_id, app_version, locale, bit)))
                        else:
                            exec_cmd("{0} -C {1}/tools/update-packaging complete-patch PACKAGE_BASE_DIR={1}/dist/l10n-stage AB_CD={2}".format(make, objdir, locale))
                        # Create the update snippet for this locale
                        prepare_updates(locale, product, branch, repo_cat, objdir, force32bit)
                except Exception as e:
                    logger.error("\n".join(traceback.format_stack()))
                    logger.error("{0}".format(traceback.format_exc()))
                finally:
                    try:
                        # clobber now, so the next locale does not get remnants of this one
                        exec_cmd("{2} -C {3}/{1}/locales clobber-zip AB_CD={0}".format(locale, target, make, objdir))
                    except Exception as e:
                        logger.error("\n".join(traceback.format_stack()))
                        logger.error("{0}".format(traceback.format_exc()))
                locale = fh.readline().strip()

    if build or l10n_repack:
        # Create the mar-file for en-US
        exec_cmd("{0} -C {1}/tools/update-packaging complete-patch".format(make, objdir))
        # Create the update snippet for en-US
        prepare_updates("en-US", product, branch, repo_cat, objdir, force32bit)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Automated build script for SeaMonkey, Thunderbird and Firefox')
    parser.add_argument('--target', action='store', default='suite', choices=['suite', 'mail', 'browser'])
    parser.add_argument('--branch', action='store', default='central', choices=['central', 'beta', 'release', 'esr'])
    parser.add_argument('--releasetag', action='store', default=None)
    parser.add_argument('--inspectortag', action='store', default=None)
    parser.add_argument('--chatzillatag', action='store', default=None)
    parser.add_argument('--debug', action='store_true', default=False)
    parser.add_argument('--calendar', action='store_true', default=False)
    parser.add_argument('--force32bit', action='store_true', default=False)
    parser.add_argument('--dontpull', action='store_true', default=False)
    parser.add_argument('--dontclobber', action='store_true', default=False)
    parser.add_argument('--dontbuild', action='store_true', default=False)
    parser.add_argument('--dontrepack', action='store_true', default=False)
    parser.add_argument('--disablepatches', action='store_true', default=False)
    args = parser.parse_args()
    main(target=args.target, branch=args.branch, release_tag=args.releasetag, debug=args.debug, calendar=args.calendar, inspector_tag=args.inspectortag, chatzilla_tag=args.chatzillatag, force32bit=args.force32bit, pull=not args.dontpull, build=not args.dontbuild, l10n_repack=not args.dontrepack, clobber=not args.dontclobber, applypatches=not args.disablepatches)
